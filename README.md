Responsive photogalery based on Adobe XD template from designer using only html and css (sass).
Here is URL link of this photogalery: https://exceptionze.gitlab.io/responsive-photogalery-based-on-adobe-xd/

Here is URL link for Adobe XD template: https://xd.adobe.com/view/e0fd51bb-8e46-42d7-8fb9-a2cd945e3e41-b5ae/

All images were free used from pexels.com